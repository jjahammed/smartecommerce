<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['as'=>'admin.','prefix'=>'admin','namespace'=>'admin','middleware'=>'auth','Middleware' =>'admin'],function(){
    Route::get('dashboard','dashboardController@index')->name('dashboard');
    Route::get('/','dashboardController@gotodashboard')->name('gotodashboard');
    Route::resource('tag','tagController');
    Route::resource('catagory','catagoryController');
    Route::resource('subcatagory','subcatagoryController');
    // Route::resource('post','postController');
    // Route::resource('subscribe','subscribeController');

    // Route::put('approaved/{approaved}','postController@approaved');
});

Route::group(['as'=>'user.','prefix'=>'user','namespace'=>'user','middleware'=>'auth','Middleware' =>'user'],function(){
    Route::get('dashboard','dashboardController@index')->name('dashboard');
    Route::get('/','dashboardController@gotodashboard')->name('gotodashboard');
    // Route::resource('post','postController');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/shop', 'frontend\shopController@index')->name('shop');
