<!-- <p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p> -->

## SMART ECOMMERCE

  SMART ECOMMERCE is a Open Source Ecommerce Plateform Developed by Laravel Framework.

## Installation Instruction

- Clone The Repo
- Run The Composer
- Copy the Files
- Run the command 'php artisan migrate --seed'

## Contributing

- Fork the Repo
- Clone the Repo Locally
- Checkout the brench
- work commit and push
- merge it

## License

The SMART ECOMMERCE is open-source software licensed under the SMART SOFTWARE LTD.(www.smartsoftware.bom.bd)).
