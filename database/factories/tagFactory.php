<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\models\Tag;
use Faker\Generator as Faker;

$factory->define(Tag::class, function (Faker $faker) {
  $tmp_name = $faker->state;
  $slug = Str::slug($tmp_name);
    return [
        'name' => $tmp_name,
        'slug' => $slug
    ];
});
