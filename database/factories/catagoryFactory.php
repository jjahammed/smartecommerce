<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\models\Catagory;
use Faker\Generator as Faker;

$factory->define(Catagory::class, function (Faker $faker) {
  $tmp_name = $faker->country;
  $slug = Str::slug($tmp_name);
    return [
        'name' => $tmp_name,
        'slug' => $slug,
        'image' => $faker->imageUrl()
    ];
});
