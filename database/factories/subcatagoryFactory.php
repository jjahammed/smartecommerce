<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\models\Catagory;
use App\models\Subcatagory;
use Faker\Generator as Faker;

$factory->define(Subcatagory::class, function (Faker $faker) {
  $catagory = Catagory::all()->random()->id;
  $tmp_name = $faker->state;
  $slug = Str::slug($tmp_name);
    return [
        'name' => $tmp_name,
        'slug' => $slug,
        'catagory_id' => $catagory,
        'image' => $faker->imageUrl()
    ];
});
