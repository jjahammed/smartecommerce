<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\models\Tag::class,30)->create();
        factory(App\models\Catagory::class,10)->create();
        factory(App\models\Subcatagory::class,30)->create();
    }
}
