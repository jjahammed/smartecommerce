@extends('layouts.fontend.master')

@section('title','Home Page')

@push('css')

@endpush

@section('mainContent')
<section class="slider_section slider_s_one  mt-20 mb-4">
      <div class="container">
          <div class="row">
              <div class="col-lg-9 col-md-12">
                  <div class="swiper-container gallery-top">
                      <div class="slider_area swiper-wrapper">
                          <div class="single_slider swiper-slide d-flex align-items-center" data-bgimg="{{ url('assest/fontend/img/slider/slider1.jpg')}}">
                              <div class="slider_content">
                                  <h3>popular products</h3>
                                  <h1>summer <br> collection 2019</h1>
                                      <p>discount <span> -30% off</span> this week</p>
                                      <a class="button" href="shop.html">DISCOVER NOW</a>
                              </div>
                          </div>
                          <div class="single_slider swiper-slide d-flex align-items-center" data-bgimg="{{ url('assest/fontend/img/slider/slider2.jpg')}}">
                              <div class="slider_content">
                                  <h3>big sale products</h3>
                                  <h1>wooden minimalist <br> chair 2019</h1>
                                      <p>discount <span> -50% off</span> this week</p>
                                      <a class="button" href="shop.html">DISCOVER NOW</a>
                              </div>
                          </div>
                          <div class="single_slider swiper-slide d-flex align-items-center" data-bgimg="{{ url('assest/fontend/img/slider/slider3.jpg')}}">
                              <div class="slider_content color_white">
                                  <h3>new arrivals</h3>
                                  <h1>business <br> off mobile apps</h1>
                                      <p>discount <span> -10% off</span> this week</p>
                                      <a class="button" href="shop.html">DISCOVER NOW</a>
                              </div>
                          </div>
                          <div class="single_slider swiper-slide d-flex align-items-center" data-bgimg="{{ url('assest/fontend/img/slider/slider4.jpg')}}">
                              <div class="slider_content color_white">
                                  <h3>new arrivals</h3>
                                  <h1>cellphone <br> new model 2019</h1>
                                      <p>discount <span> -60% off</span> this week</p>
                                      <a class="button" href="shop.html">DISCOVER NOW</a>
                              </div>
                          </div>
                      </div>
                      <!-- Add Arrows -->


                  </div>
                  <div class="swiper-container gallery-thumbs">

                  </div>

              </div>
              <div class="s_banner col-lg-3 col-md-12">
                  <!--banner area start-->
                  <div class="sidebar_banner_area">
                      <figure class="single_banner mb-20">
                          <div class="banner_thumb">
                              <a href="shop.html"><img src="{{ url('assest/fontend/img/bg/banner1.jpg')}}" alt=""></a>
                          </div>
                      </figure>
                      <figure class="single_banner mb-20">
                          <div class="banner_thumb">
                              <a href="shop.html"><img src="{{ url('assest/fontend/img/bg/banner2.jpg')}}" alt=""></a>
                          </div>
                      </figure>
                      <figure class="single_banner">
                          <div class="banner_thumb">
                              <a href="shop.html"><img src="{{ url('assest/fontend/img/bg/banner3.jpg')}}" alt=""></a>
                          </div>
                      </figure>
                  </div>
                  <!--banner area end-->
              </div>
          </div>
      </div>
  </section>
  <!--slider area end-->

  <!--shipping area start-->
     <div class="shipping_area">
      <div class="container">
          <div class=" row">

              <div class="col-lg-3 col-md-3 col-12 ">
                 <div class="col-shipping-box border">
                  <div class="shipping_content pl-4 pb-4 pt-4">
                  <i class="fa fa-id-card-o float-left pr-4 fa-2x" aria-hidden="true"></i>

                      <h4 class="pt-1">Free Delivery</h4>
                      <p>For all oders over $120</p>
                  </div>
                 </div>
              </div>

             <div class="col-lg-3 col-md-3 col-12 ">
                 <div class="col-shipping-box border">
                  <div class="shipping_content pl-4 pb-4 pt-4">
                  <i class="fa fa-id-card-o float-left pr-4 fa-2x" aria-hidden="true"></i>

                      <h4 class="pt-1">Free Delivery</h4>
                      <p>For all oders over $120</p>
                  </div>
                 </div>
              </div>

               <div class="col-lg-3 col-md-3 col-12 ">
                 <div class="col-shipping-box border">
                  <div class="shipping_content pl-4 pb-4 pt-4">
                  <i class="fa fa-id-card-o float-left pr-4 fa-2x" aria-hidden="true"></i>

                      <h4 class="pt-1">Free Delivery</h4>
                      <p>For all oders over $120</p>
                  </div>
                 </div>
              </div>

               <div class="col-lg-3 col-md-3 col-12 ">
                 <div class="col-shipping-box border">
                  <div class="shipping_content pl-4 pb-4 pt-4">
                  <i class="fa fa-id-card-o float-left pr-4 fa-2x" aria-hidden="true"></i>

                      <h4 class="pt-1">Free Delivery</h4>
                      <p>For all oders over $120</p>
                  </div>
                 </div>
              </div>
          </div>
      </div>
  </div>
  <!--shipping area end-->

  <!--home section bg area start-->
  <div class="home_section_bg mt-4">


      <!--product area start-->
      <div class="product_area deals_product">
          <div class="container">
              <div class="row">
                  <div class="col-12">
                      <div class="product_header">
                          <div class="section_title">
                              <h2>Feature Products</h2>

                          </div>
                          <div class="product_tab_btn">
                              <ul class="nav" role="tablist">
                                  <li>
                                      <a class="active" data-toggle="tab" href="#Fashion" role="tab" aria-controls="Fashion" aria-selected="true">
                                          Fashion & Clothing
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Games" role="tab" aria-controls="Games" aria-selected="false">
                                          Games & Consoles
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Speaker" role="tab" aria-controls="Speaker" aria-selected="false">
                                          Headphone & Speaker
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Mobile" role="tab" aria-controls="Mobile" aria-selected="false">
                                          Mobile & Tablets
                                      </a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="tab-content">
                  <div class="tab-pane fade show active" id="Fashion" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product1.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product2.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <!-- <span class="old_price">$86.00</span> -->
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product3.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product4.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product5.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product6.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product7.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product8.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>
                                          Health Care
                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product9.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product10.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>

                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product11.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product12.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>

                  </div>
                  <div class="tab-pane fade" id="Games" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product5.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product6.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product7.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product8.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product9.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product10.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product11.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product12.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product1.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product2.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product3.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product4.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
                  <div class="tab-pane fade" id="Speaker" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>
                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
                  <div class="tab-pane fade" id="Mobile" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>
                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
              </div>

          </div>
      </div>
      <!--product area end-->

      <!--banner area start-->
      <div class="banner_area mb-3">
          <div class="container">
              <div class="row">
                  <div class="col-lg-6 col-md-6">
                      <figure class="single_banner">
                          <div class="banner_thumb">
                              <a href="shop.html"><img src="{{ url('assest/fontend/img/bg/banner4.jpg')}}" alt=""></a>
                          </div>
                      </figure>
                  </div>
                  <div class="col-lg-6 col-md-6">
                      <figure class="single_banner">
                          <div class="banner_thumb">
                              <a href="shop.html"><img src="{{ url('assest/fontend/img/bg/banner4.jpg')}}" alt=""></a>
                          </div>
                      </figure>
                  </div>
              </div>
          </div>
      </div>
      <!--banner area end-->


<!--product area start-->
     <div class="product_area deals_product">
          <div class="container">
              <div class="row">
                  <div class="col-12">
                      <div class="product_header">
                          <div class="section_title">
                              <h2>Health cate</h2>

                          </div>
                          <div class="product_tab_btn">
                              <ul class="nav" role="tablist">
                                  <li>
                                      <a class="active" data-toggle="tab" href="#Fashion" role="tab" aria-controls="Fashion" aria-selected="true">
                                          Fashion & Clothing
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Games" role="tab" aria-controls="Games" aria-selected="false">
                                          Games & Consoles
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Speaker" role="tab" aria-controls="Speaker" aria-selected="false">
                                          Headphone & Speaker
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Mobile" role="tab" aria-controls="Mobile" aria-selected="false">
                                          Mobile & Tablets
                                      </a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="tab-content">
                  <div class="tab-pane fade show active" id="Fashion" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product3.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product4.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product1.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product2.jpg')}}.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product5.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product6.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product7.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product8.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>
                                          Health Care
                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product9.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product10.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>

                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product11.jpg')}}" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="{{ url('assest/fontend/img/product/product12.jpg')}}" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>

                  </div>
                  <div class="tab-pane fade" id="Games" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
                  <div class="tab-pane fade" id="Speaker" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>
                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
                  <div class="tab-pane fade" id="Mobile" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>
                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
              </div>

          </div>
      </div>

      <!--product area end-->
      <!--banner area start-->
      <div class="banner_area mb-3">
          <div class="container">
              <div class="row">
                  <div class="col-lg-6 col-md-6">
                      <figure class="single_banner">
                          <div class="banner_thumb">
                              <a href="shop.html"><img src="{{ url('assest/fontend/img/bg/banner4.jpg')}}" alt=""></a>
                          </div>
                      </figure>
                  </div>
                  <div class="col-lg-6 col-md-6">
                      <figure class="single_banner">
                          <div class="banner_thumb">
                              <a href="shop.html"><img src="{{ url('assest/fontend/img/bg/banner4.jpg')}}" alt=""></a>
                          </div>
                      </figure>
                  </div>
              </div>
          </div>
      </div>
      <!--banner area end-->

<!--product area start-->
    <div class="product_area deals_product">
          <div class="container">
              <div class="row">
                  <div class="col-12">
                      <div class="product_header">
                          <div class="section_title">
                              <h2>Skin Care</h2>

                          </div>
                          <div class="product_tab_btn">
                              <ul class="nav" role="tablist">
                                  <li>
                                      <a class="active" data-toggle="tab" href="#Fashion" role="tab" aria-controls="Fashion" aria-selected="true">
                                          Fashion & Clothing
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Games" role="tab" aria-controls="Games" aria-selected="false">
                                          Games & Consoles
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Speaker" role="tab" aria-controls="Speaker" aria-selected="false">
                                          Headphone & Speaker
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Mobile" role="tab" aria-controls="Mobile" aria-selected="false">
                                          Mobile & Tablets
                                      </a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="tab-content">
                  <div class="tab-pane fade show active" id="Fashion" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>
                                          Health Care
                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>

                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>

                  </div>
                  <div class="tab-pane fade" id="Games" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
                  <div class="tab-pane fade" id="Speaker" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>
                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
                  <div class="tab-pane fade" id="Mobile" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>
                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
              </div>

          </div>
      </div>

      <!--product area end-->

  <!--banner area start-->
      <div class="banner_area mb-3">
          <div class="container">
              <div class="row">
                  <div class="col-lg-6 col-md-6">
                      <figure class="single_banner">
                          <div class="banner_thumb">
                              <a href="shop.html"><img src="assets/img/bg/banner4.jpg" alt=""></a>
                          </div>
                      </figure>
                  </div>
                  <div class="col-lg-6 col-md-6">
                      <figure class="single_banner">
                          <div class="banner_thumb">
                              <a href="shop.html"><img src="assets/img/bg/banner5.jpg" alt=""></a>
                          </div>
                      </figure>
                  </div>
              </div>
          </div>
      </div>

    <div class="product_area deals_product">
          <div class="container">
              <div class="row">
                  <div class="col-12">
                      <div class="product_header">
                          <div class="section_title">
                              <h2>Food Supliments</h2>

                          </div>
                          <div class="product_tab_btn">
                              <ul class="nav" role="tablist">
                                  <li>
                                      <a class="active" data-toggle="tab" href="#Fashion" role="tab" aria-controls="Fashion" aria-selected="true">
                                          Fashion & Clothing
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Games" role="tab" aria-controls="Games" aria-selected="false">
                                          Games & Consoles
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Speaker" role="tab" aria-controls="Speaker" aria-selected="false">
                                          Headphone & Speaker
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#Mobile" role="tab" aria-controls="Mobile" aria-selected="false">
                                          Mobile & Tablets
                                      </a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="tab-content">
                  <div class="tab-pane fade show active" id="Fashion" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>
                                          Health Care
                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>

                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>

                  </div>
                  <div class="tab-pane fade" id="Games" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
                  <div class="tab-pane fade" id="Speaker" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>
                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
                  <div class="tab-pane fade" id="Mobile" role="tabpanel">
                      <div class="product_carousel product_style product_column5 owl-carousel">
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$72.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Eodem modo vel mattis ante facilisis nec porttitor efficitur</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$86.00</span>
                                              <span class="current_price">$79.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Natus erro at congue massa commodo sit Natus erro</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$80.00</span>
                                              <span class="current_price">$70.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Nullam maximus eget nisi dignissim sodales eget tempor</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$76.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>
                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Donec tempus pretium arcu et faucibus commodo</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$82.00</span>
                                              <span class="current_price">$75.00</span>
                                          </div>

                                      </div>
                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                          <article class="single_product">
                              <figure>

                                  <div class="product_thumb">
                                      <a class="primary_img" href="product-countdown.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                      <a class="secondary_img" href="product-countdown.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                      <div class="label_product">
                                          <span class="label_sale">Sale</span>
                                      </div>
                                      <div class="action_links">
                                          <ul>
                                              <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                              <li class="compare"><a href="#" title="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                              <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="product_content">
                                      <div class="product_content_inner">
                                          <h4 class="product_name"><a href="product-countdown.html">Mirum est notare tellus eu nibh iaculis pretium</a></h4>
                                          <div class="price_box">
                                              <span class="old_price">$65.00</span>
                                              <span class="current_price">$60.00</span>
                                          </div>

                                      <div class="add_to_cart">
                                          <a href="cart.html" title="Add to cart">Add to cart</a>
                                      </div>

                                  </div>
                              </figure>
                          </article>
                      </div>
                  </div>
              </div>

          </div>
      </div>
  </div>

  <!-- modal area start-->
      
      <!-- modal area end-->

      <!--news letter popup start-->
      <div class="newletter-popup">
          <div id="boxes" class="newletter-container">
              <div id="dialog" class="window">
                  <div id="popup2">
                      <span class="b-close"><span>close</span></span>
                  </div>
                  <div class="box">

                      <div class="box-content newleter-content">
                          <!-- <img src="assets/img/logo/newsletter.png" alt=""> -->
                          <img src="{{ url('assest/fontend/img/logo/newsletter.png')}}" alt="">
                      </div>
                          <!-- /#frm_subscribe -->
                      </div>
                      <!-- /.box-content -->
                  </div>
              </div>

          </div>
          <!-- /.box -->
      </div>
      <!--news letter popup start-->
@endsection


@push('js')
@endpush
