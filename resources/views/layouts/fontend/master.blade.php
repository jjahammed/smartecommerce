<!doctype html>
<html class="no-js" lang="en">


<!-- Mirrored from demo.hasthemes.com/antomi-preview/antomi/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 12 Oct 2019 04:17:31 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS
    ========================= -->

    <!-- Plugins CSS -->
    <link href="{{ asset('assest/fontend/css/plugins.css')}}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="assets/css/plugins.css"> -->

    <!-- Main Style CSS -->
    <link href="{{ asset('assest/fontend/css/style.css')}}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="assets/css/style.css"> -->
      <!-- Toster -->
      <!-- <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css"> -->
@stack('css')
</head>
<body>

  @include('layouts.fontend.partial.header')

  @section('mainContent')

  @show

  @include('layouts.fontend.partial.footer')


      <!-- JS
  ============================================ -->

      <!-- Plugins JS -->
      <script src="{{ asset('assest/fontend/js/plugins.js') }}"></script>
      <!-- <script src="assets/js/plugins.js"></script> -->

      <!-- Main JS -->
      <script src="{{ asset('assest/fontend/js/main.js') }}"></script>
      <!-- <script src="assets/js/main.js"></script> -->

    @stack('js')

  </body>
  <!-- Mirrored from demo.hasthemes.com/antomi-preview/antomi/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 12 Oct 2019 04:18:08 GMT -->
  </html>
  <!-- js SCIPTS  old -->
