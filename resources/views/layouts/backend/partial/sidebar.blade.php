<aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    @if(Request::is('admin*'))
                    <li class="{{ Request::is('admin/dashboard') ? 'active' : ''}} ">
                        <a href="{{ url('admin/dashboard')}}">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('admin/tag*') ? 'active' : ''}} ">
                        <a href="{{ url('admin/tag')}}">
                            <i class="material-icons">apps</i>
                            <span>Tags</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('admin/catagory*') ? 'active' : ''}} ">
                        <a href="{{ url('admin/catagory')}}">
                            <i class="material-icons">apps</i>
                            <span>Catagorys</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('admin/subcatagory*') ? 'active' : ''}} ">
                        <a href="{{ url('admin/subcatagory')}}">
                            <i class="material-icons">apps</i>
                            <span>Sub Catagorys</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">map</i>
                            <span>Maps</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="pages/maps/google.html">Google Map</a>
                            </li>
                            <li>
                                <a href="pages/maps/yandex.html">YandexMap</a>
                            </li>
                            <li>
                                <a href="pages/maps/jvectormap.html">jVectorMap</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">trending_down</i>
                            <span>Multi Level Menu</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="javascript:void(0);">
                                    <span>Menu Item</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <span>Menu Item - 2</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Level - 2</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <span>Menu Item</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="menu-toggle">
                                            <span>Level - 3</span>
                                        </a>
                                        <ul class="ml-menu">
                                            <li>
                                                <a href="javascript:void(0);">
                                                    <span>Level - 4</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="header">Footer</li>
                    <li>
                      <a class="dropdown-item" href="{{ route('logout')}}"
                          onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i>
                            <span>Logout</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout')}}" method="post" style="display:none">
                          @csrf
                        </form>
                    </li>
                    @endif






                    @if(Request::is('user*'))
                    <li class="{{ Request::is('user/dashboard') ? 'active' : ''}} ">
                        <a href="{{ url('user/dashboard')}}">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('user/post*') ? 'active' : ''}} ">
                        <a href="{{ url('user/post')}}">
                            <i class="material-icons">apps</i>
                            <span>Post</span>
                        </a>
                    </li>
                    <li class="header">Footer</li>
                    <li>
                      <a class="dropdown-item" href="{{ route('logout')}}"
                          onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i>
                            <span>Logout</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout')}}" method="post" style="display:none">
                          @csrf
                        </form>
                    </li>

                    @endif
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 - 2020 <a href="javascript:void(0);">Imtiaj ahammed - jjahammed@gmail.com</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 01798596317
                </div>
            </div>
            <!-- #Footer -->
        </aside>
