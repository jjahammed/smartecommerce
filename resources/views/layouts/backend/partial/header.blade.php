<div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->

    <!-- #END# Search Bar -->
    <style media="screen">
      .ppicture img{
        width: 45px;
        height: 45px;
        border-radius:50%;
        margin-top: 10px;
        cursor: pointer;
      }
    </style>
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">SMART - ECOMMERCE</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <!-- <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li> -->
                    <!-- #END# Call Search -->
                    <!-- Notifications -->

                    <!-- #END# Notifications -->
                    <!-- Tasks -->

                    <!-- #END# Tasks -->
                    <!-- <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li> -->

                        <li class="pull-right ppicture">

                          <div class="user-helper-dropdown">
                            <img src="{{asset('assest/backend/images/user.png')}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" alt="User" />
                              <!-- <i class="material-icons" >keyboard_arrow_down</i> -->
                              <ul class="dropdown-menu pull-right mb-5">
                                  <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                                  <li role="separator" class="divider"></li>
                                  <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                                  <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                                  <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                                  <li role="separator" class="divider"></li>
                                  <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                              </ul>
                          </div>

                        </li>

                </ul>
            </div>
        </div>
    </nav>
