@if(auth::user()->role == 2)

@else
<script>window.location = "/admin/dashboard";</script>
@endif

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

	<!-- Stylesheets -->

<!-- Bootstrap Core Css -->
<link href="{{ asset('assest/backend/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

<!-- Waves Effect Css -->
<link href="{{ asset('assest/backend/plugins/node-waves/waves.css')}}" rel="stylesheet" />

<!-- Animation Css -->
<link href="{{ asset('assest/backend/plugins/animate-css/animate.css')}}" rel="stylesheet" />

<!-- Sweet Alert Css -->
  <link href="{{asset('assest/backend/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" />
<!-- Morris Chart Css-->
<link href="{{ asset('assest/backend/plugins/morrisjs/morris.css')}}" rel="stylesheet" />

<!-- Custom Css -->
<link href="{{ asset('assest/backend/css/style.css')}}" rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="{{ asset('assest/backend/css/themes/all-themes.css')}}" rel="stylesheet" />
<!-- Toster -->
<link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
@stack('css')
</head>
<body class="theme-blue">

  @include('layouts.backend.partial.header')
  @include('layouts.backend.partial.sidebar')

@section('mainContain')

@show
  <!-- SCIPTS -->

  <!-- Jquery Core Js -->
<script src="{{ asset('assest/backend/plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Core Js -->
<script src="{{ asset('assest/backend/plugins/bootstrap/js/bootstrap.js')}}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{ asset('assest/backend/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{ asset('assest/backend/plugins/node-waves/waves.js')}}"></script>


<!-- Custom Js -->
<script src="{{ asset('assest/backend/js/admin.js')}}"></script>


<!-- Demo Js -->
<script src="{{ asset('assest/backend/js/demo.js')}}"></script>
<!-- Toster -->
<script src="https://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
{!! Toastr::message() !!}
<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
<!-- Sweet alert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>


  @stack('js')
</body>
</html>
