@extends('layouts.backend.admin.master')
@section('title','Tag List')

@push('css')
<link href="asset('assest/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')" rel="stylesheet">
@endpush

@section('mainContain')
<section class="content">
        <div class="container-fluid">

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                TAG LIST
                                <span class="badge bg-primary">{{$show_data->count()}}</span>
                                <a href="{{ url('/admin/tag/create')}}" class="btn btn-secondary waves-effect pull-right" style="margin-top:-7px;"><i class="material-icons">add</i></a>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                  <thead class="text-center">
                                      <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>slug</th>
                                        <th>Post Count</th>
                                        <th>Action</th>

                                      </tr>
                                  </thead>
                                  <tfoot class="text-center">
                                      <tr>
                                          <th>ID</th>
                                          <th>Name</th>
                                          <th>slug</th>
                                          <th>Post Count</th>
                                          <th>Action</th>

                                      </tr>
                                  </tfoot>
                                  <tbody>
                                    @foreach ($show_data as $key => $data)
                                    <tr>
                                      <td>{{$key +1}}</td>
                                      <td>{{$data->name}}</td>
                                      <td>{{$data->slug}}</td>
                                      <td class="text-center">

                                      </td>
                                      <td class="text-center">
                                        <a href="/admin/tag/{{$data->id}}/edit" class="btn btn-success d-inline"><i class="material-icons">edit</i></a>
                                        <a class="btn btn-danger waves-effect" onclick="delData({{$data->id}})"><i class="material-icons">delete</i></a>

                                      <form id="delete-form-{{$data->id}}" class="" action="/admin/tag/{{$data->id}}" method="post" style="Display:none">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                      </form>

                                      </td>
                                    </tr>
                                    @endforeach
                                  </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
@endsection

@push('js')
<!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assest/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>

<script src="{{asset('assest/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assest/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assest/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{asset('assest/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{asset('assest/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{asset('assest/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{asset('assest/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{asset('assest/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
<script src="{{asset('assest/backend/js/pages/tables/jquery-datatable.js')}}"></script>

<script>
function delData(id){
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })

  swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      swalWithBootstrapButtons.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success',
        event.preventDefault(),
        document.getElementById('delete-form-'+id).submit()
      )
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons.fire(
        'Cancelled',
        'Your imaginary file is safe :)',
        'error'
      )
    }
  })
}
</script>


@endpush
