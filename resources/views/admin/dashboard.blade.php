@extends('layouts.backend.admin.master')

@section('title','Admin Dashboard')

@push('css')
@endpush

@section('mainContain')
<section class="content">
      <div class="container-fluid">
          <div class="block-header">
              <h2>Admin <strong>{{auth::user()->name}}'s and Role is : {{auth::user()->role}} </strong>DASHBOARD</h2>
          </div>

          <!-- Widgets -->
          <div class="row clearfix">
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-pink hover-expand-effect">
                      <div class="icon">
                          <i class="material-icons">playlist_add_check</i>
                      </div>
                      <div class="content">
                          <div class="text">NEW TASKS</div>
                          <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"></div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-cyan hover-expand-effect">
                      <div class="icon">
                          <i class="material-icons">help</i>
                      </div>
                      <div class="content">
                          <div class="text">NEW TICKETS</div>
                          <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"></div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-light-green hover-expand-effect">
                      <div class="icon">
                          <i class="material-icons">forum</i>
                      </div>
                      <div class="content">
                          <div class="text">NEW COMMENTS</div>
                          <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"></div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-orange hover-expand-effect">
                      <div class="icon">
                          <i class="material-icons">person_add</i>
                      </div>
                      <div class="content">
                          <div class="text">NEW VISITORS</div>
                          <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"></div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Widgets -->
          <!-- Main Section -->
          <div class="row clearfix">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="card">
                      <div class="header">
                        <h2>Main Section</h2>
                       </div>
                      <div class="body">
                          <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# main Section -->
      </div>
  </section>
@endsection

@push('js')
<!-- Jquery CountTo Plugin Js -->
<script src="{{ asset('assest/backend/plugins/jquery-countto/jquery.countTo.js')}}"></script>

<!-- Morris Plugin Js -->
<script src="{{ asset('assest/backend/plugins/raphael/raphael.min.js')}}"></script>
<script src="{{ asset('assest/backend/plugins/morrisjs/morris.js')}}"></script>

<!-- ChartJs -->
<script src="{{ asset('assest/backend/plugins/chartjs/Chart.bundle.js')}}"></script>

<!-- Flot Charts Plugin Js -->
<script src="{{ asset('assest/backend/plugins/flot-charts/jquery.flot.js')}}"></script>
<script src="{{ asset('assest/backend/plugins/flot-charts/jquery.flot.resize.js')}}"></script>
<script src="{{ asset('assest/backend/plugins/flot-charts/jquery.flot.pie.js')}}"></script>
<script src="{{ asset('assest/backend/plugins/flot-charts/jquery.flot.categories.js')}}"></script>
<script src="{{ asset('assest/backend/plugins/flot-charts/jquery.flot.time.js')}}"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="{{ asset('assest/backend/plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>

<script src="{{ asset('assest/backend/js/pages/index.js')}}"></script>
@endpush
