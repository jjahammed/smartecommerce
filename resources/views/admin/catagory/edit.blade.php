@extends('layouts.backend.admin.master')

@section('title','Update Catagory')

@push('css')
@endpush

@section('mainContain')
<section class="content">
      <div class="container-fluid">
          <div class="block-header">
              @if($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                  <ul>
                    <li>{{ $error }}</li>
                  </ul>
                </div>
                @endforeach
              @endif

          </div>


          <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                UPDATE CATAGORY
                            </h2>
                        </div>
                        <div class="body">
                              <form class="" action="/admin/catagory/{{$edit_data->id}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{method_field('PUT')}}
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="name" id="tagName" class="form-control" value="{{$edit_data->name }}">
                                        <label class="form-label">Enter Catagory Name</label>
                                    </div>
                                </div>

                                <div class="form-group form-float">
                                    <div class="">
                                        <input type="file" name="image" id="" class="form-control">
                                    </div>
                                </div>

                                <input type="submit" name="" class="btn btn-primary m-t-15 waves-effect"  value="SUBMIT">
                                <a href="{{ url('admin/catagory')}}" class="btn btn-primary m-t-15 waves-effect">BACK</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
      </div>
  </section>
@endsection

@push('js')
@endpush
