@extends('layouts.backend.admin.master')

@section('title','Sub Catagory Create')

@push('css')
<link href="{{ asset('assest/backend/plugins/multi-select/css/multi-select.css')}}" rel="stylesheet">
<link href="{{ asset('assest/backend/plugins/bootstrap-select/css/bootstrap-select.css' )}}" rel="stylesheet" />
@endpush

@section('mainContain')
<section class="content">
      <div class="container-fluid">
          <div class="block-header">
              @if($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                  <ul>
                    <li>{{ $error }}</li>
                  </ul>
                </div>
                @endforeach
              @endif

          </div>

          <!-- form section -->
          <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                CREATE SUB CATAGORY
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ url('/admin/subcatagory')}}" method="post" enctype="multipart/form-data">
                              {{csrf_field()}}
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="name" id="catagoryName" class="form-control">
                                        <label class="form-label">Enter catagory Name</label>
                                    </div>
                                </div>

                                <div class="form-group form-float">
                                    <select class="form-control show-tick" name="catagory">
                                        <option value="">-- Please select --</option>
                                        @foreach ($catagory as $cats)
                                          <option value="{{$cats->id}}">{{$cats->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group form-float">
                                    <div class="">
                                        <input type="file" name="image" id="" class="form-control">
                                    </div>
                                </div>

                                <input type="submit" name="" class="btn btn-primary m-t-15 waves-effect"  value="SUBMIT">
                                <a href="{{ url('admin/subcatagory')}}" class="btn btn-primary m-t-15 waves-effect">BACK</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- end  form section -->
      </div>
  </section>
@endsection

@push('js')
<!-- Select plugins -->
<script src="{{ asset('assest/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
<!-- multi select plugins -->
<script src="{{ asset('assest/backend/plugins/multi-select/js/jquery.multi-select.js' )}}"></script>
@endpush
