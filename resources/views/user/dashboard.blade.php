@extends('layouts.backend.user.master')

@section('title','User Dashboard')

@push('css')
@endpush

@section('mainContain')
<section class="content">
      <div class="container-fluid">
          <div class="block-header">
              <h2>User <strong>{{auth::user()->name}}'s and Role is : {{auth::user()->role}} </strong>DASHBOARD</h2>
          </div>

          <!-- Widgets -->
          <div class="row clearfix">
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-pink hover-expand-effect">
                      <div class="icon">
                          <i class="material-icons">playlist_add_check</i>
                      </div>
                      <div class="content">
                          <div class="text">NEW TASKS</div>
                          <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"></div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-cyan hover-expand-effect">
                      <div class="icon">
                          <i class="material-icons">help</i>
                      </div>
                      <div class="content">
                          <div class="text">NEW TICKETS</div>
                          <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"></div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-light-green hover-expand-effect">
                      <div class="icon">
                          <i class="material-icons">forum</i>
                      </div>
                      <div class="content">
                          <div class="text">NEW COMMENTS</div>
                          <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"></div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-orange hover-expand-effect">
                      <div class="icon">
                          <i class="material-icons">person_add</i>
                      </div>
                      <div class="content">
                          <div class="text">NEW VISITORS</div>
                          <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"></div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Widgets -->
          <!-- Main Section -->
          <div class="row clearfix">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="card">
                      <div class="header">
                        <h2>Main Section</h2>
                       </div>
                      <div class="body">
                          <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Main Section -->
      </div>
  </section>
@endsection

@push('js')
@endpush
