<?php

namespace App\Http\Controllers\admin;

use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\models\Subcatagory;
use App\models\Catagory;
use Validator;

class subcatagoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $catagory = Catagory::all();
        $show_data = Subcatagory::orderBy('id','desc')->get();
        return view('admin.subcatagory.index',compact('show_data','catagory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $catagory = Catagory::all();
        return view('admin.subcatagory.create',compact('catagory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'name' => 'required|unique:tags',
        'image' => 'required|mimes:jpg,bnp,png,jpeg,tiff'
      ]);

      $file = $request->file('image');
      if($request->hasFile('image')) {
        //get filename with extension
        $filenamewithextension = $file->getClientOriginalName();
        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        //get file extension
        $extension = $file->getClientOriginalExtension();
        //filename to store
        $filenametostore = "SUBCATAGORY_".uniqid().".".$extension;
        //Upload File
        $file->storeAs('public/Subcatagory', $filenametostore);
        //Resize image here
        $normalpath = public_path('storage/Subcatagory/'.$filenametostore);
        $img = Image::make($normalpath)->resize(350, 350);
        $img->save($normalpath);

      //  Storage::disk('public')->delete('Catagory/CATAGORY_5da5f15b0c9cd.jpg');
    }
        $ins_data = new Subcatagory();
        $ins_data->name = $request->name;
        $ins_data->catagory_id = $request->catagory;
        $ins_data->slug = str::slug($request->name);
        $ins_data->image = $filenametostore;
        $is_saved = $ins_data->save();
        if($is_saved){
          Toastr::success('Sub Catagory  Added Successcully', 'success');
          return redirect('admin/subcatagory');
    }
  }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dis_data = Subcatagory::find($id);
        return view('admin.subcatagory.show',compact('dis_data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $catagory = Catagory::all();
        $edit_data = Subcatagory::find($id);
        return view('admin.subcatagory.edit',compact('edit_data','catagory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'name' => 'required',
        'image' => 'mimes:jpg,bnp,png,jpeg,tiff'
      ]);


      $upd_data = Subcatagory::find($id);

      $upd_data->name = $request->name;
      $upd_data->catagory_id = $request->catagory;
      $upd_data->slug = str::slug($request->name);

      $file = $request->file('image');
      if($request->hasFile('image')) {
        //Delete Old files
        Storage::disk('public')->delete('Subcatagory/'.$upd_data->image);
        //get filename with extension
        $filenamewithextension = $file->getClientOriginalName();
        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        //get file extension
        $extension = $file->getClientOriginalExtension();
        //filename to store
        $filenametostore = "SUBCATAGORY_".uniqid().".".$extension;
        //Upload File
        $file->storeAs('public/Subcatagory', $filenametostore);
        //Resize image here
        $normalpath = public_path('storage/Subcatagory/'.$filenametostore);
        $img = Image::make($normalpath)->resize(350, 350);
        $img->save($normalpath);
        $upd_data->image = $filenametostore;

      }
      $is_updated = $upd_data->save();
      if($is_updated){
        Toastr::success('Sub Catagory  updated Successcully', 'success');
        return redirect('admin/subcatagory');
      }
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $del_data = Subcatagory::find($id);

    if(Storage::disk('public')->exists('Subcatagory/'.$del_data->image)){
      Storage::disk('public')->delete('Subcatagory/'.$del_data->image);
    }
      $is_deleted = $del_data->delete();
      if($is_deleted){
        Toastr::error('Sub Catagory  Deteted Successcully', 'success');
        return redirect('admin/subcatagory');
      }
    }
}
