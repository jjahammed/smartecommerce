<?php

namespace App\Http\Controllers\admin;

use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\models\Tag;
use Validator;

class tagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $show_data = Tag::orderBy("id","desc")->get();
        return view('admin.tag.index',compact('show_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $this->validate($request,[
        'name' => 'required|unique:tags'
      ]);
      /*
      $checkData = Validator::make($request->all(),[
        "name"=>"required | max:255"],[
        "name.required" => "You should enter your name" ])->validate();
*/
        $ins_data = new Tag();
        $ins_data->name = $request->name;
        $ins_data->slug = Str::slug($request->name);
        $is_saved = $ins_data->save();

        if($is_saved){
          Toastr::success('Tag  Added Successcully', 'success');
          return redirect('admin/tag');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $dis_data = Tag::find($id);
      return view('admin.tag.show',compact('dis_data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_data = Tag::find($id);
        return view('admin.tag.edit',compact('edit_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'name' => 'required'
      ]);

      $upd_data = Tag::find($id);
      $upd_data->name = $request->name;
      $upd_data->slug = Str::slug($request->name);
      $is_updated = $upd_data->save();
        if($is_updated){
          Toastr::success('Tag  Updated Successcully', 'success');
          return redirect('admin/tag');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $del_data = Tag::find($id)->delete();
      if($del_data){
        Toastr::success('Tag  Deteted Successcully', 'success');
        return redirect('admin/tag');
      }
    }
}
